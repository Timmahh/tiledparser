﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    class Image
    {
        public string Format;
        public int ID;
        public string Source;
        public string Trans;
        public int Width;
        public int Height;

        public List<Data> Data;
    }
}
