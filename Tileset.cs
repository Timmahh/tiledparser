﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    class TileSet
    {
        public int FirstGID;
        public string Source;
        public string Name;
        public int TileWidth;
        public int TileHeight;
        public int Spacing;
        public int Margin;

        public List<TileOffset> TileOffsets;
        public List<Properties> Properties;
        public List<Image> Images;
        public List<TerrainTypes> TerrainTypes;
        public List<Tile> Tiles;
    }
}
