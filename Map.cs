﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    public enum Orientation
    {
        ORTHOGONAL,
        ISOMETRIC,
        STAGGERED
    }

    class Map
    {
        public float Version;
        public Orientation Orientation;
        public int Width;
        public int Height;
        public int TileWidth;
        public int TileHeight;

        public List<Properties> Properties;
        public List<TileSet> TileSets;
        public List<Layer> Layers;
        public List<ObjectGroup> ObjectGroups;
        public List<ImageLayer> ImageLayers;
    }
}
