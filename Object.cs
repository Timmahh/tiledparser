﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace TiledParser
{
    public enum Shape
    {
        ELLIPSE, POLYGON, POLYLINE
    }

    class Object
    {
        public string Name;
        public string Type;
        public int x;
        public int y;
        public int Width;
        public int Height;
        public int Rotation;
        public int ID;
        public bool Visible;

        public List<Properties> Properties;
        public List<Object> Objects;
        public List<Vector2> Points;
    }
}
