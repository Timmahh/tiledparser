﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    class Program
    {
        static void Main(string[] args)
        {
            TmxReader reader = new TmxReader("Tilemap.tmx");

            reader.LoadFile();

            Console.ReadKey();
        }
    }
}
