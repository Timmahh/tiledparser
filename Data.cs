﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    public enum Encoding
    {
        BASE64, XML, CSV
    }

    public enum Compression
    {
        GZIP, ZLIB
    }

    class Data
    {
        public Encoding Encoding;
        public Compression Compression;

        public List<Tile> Tiles;
    }
}
