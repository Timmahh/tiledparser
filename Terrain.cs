﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    class Terrain
    {
        public string Name;
        public int Tile;

        public List<Properties> Properties;
    }
}
