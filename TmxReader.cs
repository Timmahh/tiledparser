﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TiledParser
{
    class TmxReader
    {
        private string _tmxFile;
        public string TmxFile
        {
            get { return _tmxFile; }
        }

        public TmxReader(string file)
        {
            _tmxFile = file;
        }

        public void LoadFile()
        {
            XDocument xmlFile = XDocument.Load(_tmxFile);
            XElement mapElement = xmlFile.Element("map");

            Console.WriteLine(mapElement.Attribute("orientation").Value);
            
        }
    }
}
