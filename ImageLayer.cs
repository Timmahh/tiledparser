﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    class ImageLayer
    {
        public string Name;
        public int Width;
        public int Height;
        public float Opacity;
        public bool Visible;

        public List<Properties> Properties;
        public List<Image> Images;
    }
}
