﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    class ObjectGroup
    {
        public string Name;
        public string Color;
        public int x;
        public int y;
        public int Width;
        public int Height;
        public float Opacity;
        public bool Visible;

        public List<Properties> Properties;
        public List<TiledParser.Object> Objects;
    }
}
