﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledParser
{
    class Tile
    {
        public int ID;
        public string Terrain;
        public float Probability;

        public List<Properties> Properties;
        public List<Image> Image;
    }
}
